var container, stats;

var camera, scene, renderer;

var cube, plane;

var targetRotation = 0;
var targetRotationOnMouseDown = 0;

var mouseX = 0;
var mouseXOnMouseDown = 0;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
var controlas;

init();
animate();

function init() {

	container = document.createElement( 'div' );
	document.body.appendChild( container );

	var info = document.createElement( 'div' );
	info.style.position = 'absolute';
	info.style.top = '10px';
	info.style.width = '100%';
	info.style.textAlign = 'center';
	info.innerHTML = 'Z Buffer Expo';
	container.appendChild(info);
	//Creando el html para las instrucciones
	var instructionsDiv = document.createElement('div');
	instructionsDiv.style.position = 'absolute';
	instructionsDiv.style.top = '50px';
	instructionsDiv.style.width = '100%';
	instructionsDiv.style.textAlign = 'center';
	instructionsDiv.innerHTML= 'Use WASD(x,y) QE(z) to move the cube.'
	container.appendChild( instructionsDiv );

	camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 1000 );
	camera.position.y = 450;
	camera.position.z = 500;
	controls = new THREE.OrbitControls( camera );
	scene = new THREE.Scene();
	scene.background = new THREE.Color( 0xf0f0f0 );


	//Creating Cube A
	var geometry = new THREE.BoxGeometry( 200, 200, 220,2,2,2 );

	var textura_crate = new THREE.TextureLoader().load('/src/crate.png');
	var textura_brick = new THREE.TextureLoader().load('/src/brick.png');
	var material = new THREE.MeshBasicMaterial({ map: textura_brick });
	cube = new THREE.Mesh( geometry, material );
	cube.position.z = 200;
	scene.add( cube );


	//Creating Cube B
	var geometry_b = new THREE.BoxGeometry( 200, 200, 200,2,2,2 );


	var material_b = new THREE.MeshBasicMaterial({ map: textura_crate });

	cube_b = new THREE.Mesh( geometry_b, material_b );
	cube_b.position.z = 200;
	scene.add( cube_b );


	// onclick: set color
	material.color.set(0xff0000);

	// Plane (Shadow)

	// var geometry = new THREE.PlaneBufferGeometry( 200, 200 );
	// geometry.rotateX( - Math.PI / 2 );

	// var material = new THREE.MeshBasicMaterial( { color: 0xe0e0e0, overdraw: 0.5 } );

	// plane = new THREE.Mesh( geometry, material );
	// scene.add( plane );

	renderer = new THREE.CanvasRenderer();
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	container.appendChild( renderer.domElement );

	stats = new Stats();
	container.appendChild( stats.dom );

	document.addEventListener('keydown', keyPressedListener, false);
	//

	window.addEventListener( 'resize', onWindowResize, false );

}


//Events

function onWindowResize() {

	windowHalfX = window.innerWidth / 2;
	windowHalfY = window.innerHeight / 2;

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );

}


function keyPressedListener (event) {
	console.log('keyPressed presionada..')
	var keyPressed = event.code;
	if (keyPressed == 'KeyS') {	
		cube_b.position.z += 5;
	} else if (keyPressed == 'KeyW') {
		cube_b.position.z -= 5;	
	} else if (keyPressed == 'KeyA') {
		cube_b.position.x += 5;
	} else if (keyPressed == 'keydown') {
		
	}


}


//

function animate() {

	requestAnimationFrame( animate );

	stats.begin();
	controls.update();
	renderer.render( scene, camera );
	stats.end();

}


