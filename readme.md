#Z Buffer 3D Demo

Small project to test depth in 3D.

[Based on this example from Three.js documentation.](https://threejs.org/examples/canvas_geometry_cube.html)

![Alt text](/src/demo.png?raw=true "Screenshot of running demo.")

##Requirements
Make sure you are able to run a local http server in the root of the project.
[More information about this issue](https://threejs.org/docs/#manual/introduction/How-to-run-things-locally)

###Installation

You need to run a local server on the root of the directory.
```python
	#Start the server with this code.
	#Run it in the same dir as index.html
	python3 -m http.server

```
	
	



## Acknowledgments

* Hat tip to anyone who's code was used
* Three.js developers.
